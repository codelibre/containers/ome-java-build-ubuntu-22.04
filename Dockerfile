FROM ubuntu:22.04
MAINTAINER rleigh@codelibre.net

RUN apt-get update \
  && DEBIAN_FRONTEND=noninteractive apt-get install -y -qq --no-install-recommends \
    build-essential \
    ant \
    ant-optional \
    curl \
    findbugs \
    git \
    jq \
    locales \
    man \
    maven \
    maven-ant-helper \
    openjdk-11-jdk \
    openjdk-18-jdk \
    zip \
    unzip \
    python3.10 \
    python3-genshi \
    python3-sphinx \
    python3-six \
  && locale-gen en_US.UTF-8

ENV LC_ALL=en_US.UTF-8
ENV LANG=en_US.UTF-8
